# Data Serialization in Unity

So you are looking for information about data serialization in Unity? Great! In this repository I'm giving an example how you can do it using Unity [JsonUtility](https://docs.unity3d.com/ScriptReference/JsonUtility.html) class.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/08/data-serialization-in-unity-json-friendly/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement data serialization and later save it or load it using files or PlayerPrefs.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/dataserialization/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about data serialization in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com

