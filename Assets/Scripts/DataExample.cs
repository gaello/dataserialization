﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is just an example of Data Model that you can create.
/// It will be converted from and to JSON thanks to JsonUtility class.
/// </summary>
[System.Serializable]
public class DataExample
{
    [System.Serializable]
    public class ArrayItemData
    {
        public string someMoreTest;
    }

    public float someFloat;
    public int someInt;

    public string someText;

    // We can even serialize other data objects inside data objects!
    public List<ArrayItemData> array;
}
