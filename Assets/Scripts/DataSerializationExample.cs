﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// This class contains example of handling data serialization and deserialization using JsonUtility class.
/// You can find here saving and loading from PlayerPrefs and Files.
/// </summary>
public class DataSerializationExample : MonoBehaviour
{
    /// <summary>
    /// Unity method called in first frame.
    /// </summary>
    void Start()
    {
        // Creating object with some data.
        var data = new DataExample()
        {
            someInt = Random.Range(0, 10),
            someFloat = Random.Range(0.0f, 10.0f),
            someText = "String value example",
            array = new List<DataExample.ArrayItemData>() { new DataExample.ArrayItemData() { someMoreTest = "More text in array" }, new DataExample.ArrayItemData() { someMoreTest = "And one more" } }
        };

        // Serializing data object to Json - string.
        var serialized = JsonUtility.ToJson(data);

        // Showing serialized data in console.
        Debug.LogFormat("Data serialized to:\n{0}", serialized);

        // Deserialization of string into data object again.
        var deserialized = JsonUtility.FromJson<DataExample>(serialized);
    }

    #region [Player Prefs]

    /// <summary>
    /// Method that saves json in PlayerPrefs under provided key.
    /// </summary>
    /// <param name="key">Key for PlayerPrefs.</param>
    /// <param name="json">Data in Json format.</param>
    public void SaveDataToPlayerPrefs(string key, string json)
    {
        PlayerPrefs.SetString(key, json);
    }

    /// <summary>
    /// Method that loads json from PlayerPrefs from provided key. 
    /// </summary>
    /// <returns>Data in Json format.</returns>
    /// <param name="key">Key for PlayerPrefs.</param>
    public string LoadDataFromPlayerPrefs(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetString(key);
        }

        Debug.LogErrorFormat("There is no data in PlayerPrefs under key: {0}", key);
        return "";
    }

    #endregion

    #region [IO]

    /// <summary>
    /// Method that saves json in File under provided path.
    /// </summary>
    /// <param name="path">Path to file.</param>
    /// <param name="json">Data in Json format.</param>
    public void SaveDataToFile(string path, string json)
    {
        var filePath = Path.Combine(Application.persistentDataPath, path);

        File.WriteAllText(path, json);
    }

    /// <summary>
    /// Method that loads json from File under provided path. 
    /// </summary>
    /// <returns>Data in Json format.</returns>
    /// <param name="path">Path to file.</param>
    public string LoadDataFromFile(string path)
    {
        var filePath = Path.Combine(Application.persistentDataPath, path);
        if (File.Exists(filePath))
        {
            return File.ReadAllText(filePath);
        }

        Debug.LogErrorFormat("There is no file under path: {0}", filePath);
        return "";
    }

    #endregion
}
